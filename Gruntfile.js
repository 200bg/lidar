
module.exports = function (grunt) {
    grunt.initConfig({
        '6to5': {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'lib/lidar.build.js': 'src/lidar/index.js'
                }
            }
        },
        'uglify': {
            dist: {
                files: {
                    'dist/lidar.js': 'lib/lidar.build.js'
                }
            }
        },
        less: {
          dist: {
            files: {
              'static/css/lidar.css': 'static/css/lidar.less'
            }
          }
        },
        connect: {
            server: {
                options: {
                    port: 8000,
                    useAvailablePort: true,
                    base: '.',
                    keepalive: false,
                }
            },
        },
        watch: {
          scripts: {
            files: ['src/**/*.js', 'static/css/*.less'],
            tasks: ['6to5', 'less'],
            options: {
              spawn: false,
            },
          },
        },
    });

    grunt.loadNpmTasks('grunt-6to5');
    // grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('default', ['6to5']);
    grunt.registerTask('server', ['connect', 'watch']);
    // grunt.registerTask('default', ['6to5', 'uglify']);

};
