
"use strict";
try {
  if (exports == undefined) {
    exports = window;
  }
} catch (err) {
  null;
}

var _TAU = Math.PI * 2;
var _PI = _TAU / 2;
var _Q = _TAU / 4;

var defaultConfig = {
  min: null, // null for auto
  max: null, // null for auto
  padding: 0.1, // 10% of max-min
  lineColor: 'rgba(68,49,51,0.5)',
  lineGlowColor: 'rgba(68,49,51,0.5)',
  outerLineWidth: 2,
  lineWidth: 1,
  fillColor: 'rgba(94, 94, 94, 0.15)',
  labelColor: 'rgba(255,94,94,1.0)',
  rotationOffset: _Q*3,
  labelFont: '12px Arial',
  labelPadding: 10,
  valueColor: 'rgba(255,255,255,1.0)',
}

var ChartDataSet = function() {
  var ChartDataSet = function ChartDataSet(name, dataSet, colorConfig) {
    this.name = name;
    this.dataSet = dataSet;
    this.colorConfig = colorConfig;
  };

  Object.defineProperties(ChartDataSet.prototype, {
    fields: {
      get: function() {
        if (!this._fields) {
          this._fields = [];
          for (var k in this.dataSet) {
            this._fields.push(k);
          }
        }
        return this._fields;
      }
    }
  });

  return ChartDataSet;
}();

var DataDescriptor = function() {
  var DataDescriptor = function DataDescriptor(min, max, unit) {
    this.min = min;
    this.max = max;
    // this could be a format
    // TODO: datetime formatting
    this.unit = unit;
    this.angle = 0;
  };

  return DataDescriptor;
}();

var LidarLabel = function() {
  var LidarLabel = function LidarLabel(parentElement, name, chart) {
    var i, data, value, descriptor, config, label, dataElement, commaElement;

    this.parentElement = parentElement;
    this.element = document.createElement('div');
    this.labelElement = document.createElement('div');
    this.dataContainerElement = document.createElement('div');

    this.element.classList.add('lidar-label');
    this.labelElement.classList.add('lidar-label-name');
    this.dataContainerElement.classList.add('lidar-label-data');

    this.element.classList.add('animated-out');

    this.element.appendChild(this.labelElement);
    this.element.appendChild(this.dataContainerElement);

    this.element.style.position = 'absolute';

    // add the label name
    this.labelElement.innerHTML = name;

    this.parentElement.appendChild(this.element);

    for (i = 0; i < chart.dataSets.length; i++) {
      dataElement = document.createElement('span');
      data = chart.dataSets[i];
      value = data.dataSet[name];
      descriptor = chart.dataDescriptor[name];
      config = data.colorConfig;
      dataElement.style.color = config.lineColor;
      label = value.toString() + descriptor.unit;
      dataElement.innerHTML = label;
      this.dataContainerElement.appendChild(dataElement);
      if (i < chart.dataSets.length-1) {
        commaElement = document.createElement('span');
        commaElement.innerHTML = ', ';
        this.dataContainerElement.appendChild(commaElement);
      }
    }
  };

  Object.defineProperties(LidarLabel.prototype, {
    setPosition: {
      writable: true,

      value: function(point) {
        this.element.style.left = point.x + 'px';
        this.element.style.top = point.y + 'px';
      }
    }
  });

  return LidarLabel;
}();

var Lidar = function() {
  var Lidar = function Lidar(canvas, dataSets, config, dataDescriptor) {
    if (config === null) config = {};

    this.width = canvas.width;
    this.height = canvas.height;
    if (this.width > this.height) {
      this.radius = this.height/2;
    } else {
      this.radius = this.width/2;
    }
    this.graphWidth = this.radius*0.85;
    this.graphHeight = this.radius*0.85;
    this.center = {x: Math.floor(this.width/2), y: Math.floor(this.height/2)};
    this.canvas = canvas;
    this.config = Object.assign(defaultConfig, config);

    this.ctx = this.canvas.getContext('2d');

    this.dataDescriptor = dataDescriptor || {};

    this.allFields = [];
    this.labels = {};

    this.dataSets = dataSets;

  };

  Object.defineProperties(Lidar.prototype, {
    dataSets: {
      set: function(value) {
        var i, j, label, set, data, descriptor, min, max;

        this._dataSets = value;
        // fill in the allFields list
        this.allFields = [];
        for (i = this._dataSets.length - 1; i >= 0; i--) {
          data = this._dataSets[i];

          for (label in data.dataSet) {
            value = data.dataSet[label];
            // track the fields
            if (this.allFields.indexOf(label) < 0) {
              this.allFields.push(label);
            }
          }
        }

        // now get the min/max
        for (i = 0; i < this.allFields.length; i++) {
          label = this.allFields[i];

          min = Number.MAX_VALUE;
          max = Number.MIN_VALUE;

          for (j = this._dataSets.length - 1; j >= 0; j--) {
            data = this._dataSets[j];

            value = data.dataSet[label];


            min = Math.min(value, min);
            max = Math.max(value, max);
          }

          if (max == Number.MIN_VALUE) {
            max = 0;
          }
          if (min == Number.MAX_VALUE) {
            min = 0;
          }

          if (this.config.padding) {
            max += (max-min) * this.config.padding;
          }
          // set the min max
          try {
            descriptor = this.dataDescriptor[label];
            if (descriptor.min === null) {
              descriptor.min = min;
            }
            if (descriptor.max === null) {
              descriptor.max = max;
            }

          } catch (err) {
            descriptor = this.dataDescriptor[label] = new DataDescriptor(min, max, '');
          }
        }

        this.createLabels();

        this.draw(1.0);
      },

      get: function() {
        return this._dataSets;
      }
    },

    createLabels: {
      writable: true,

      value: function() {
        var i, j, label, field, data, value, descriptor, config;

        for (i = 0; i < this.allFields.length; i++) {
          field = this.allFields[i];
          label = new LidarLabel(this.canvas.parentNode, field, this);
          console.log(field);
          this.labels[field] = label;
        }
      }
    },

    draw: {
      writable: true,

      value: function(scale) {
        var i, j, x, y, data, label, descriptor, value, min, max, radiusX, radiusY, pX, pY;

        // draw the chart
        this.drawChart(this.config.outerLineWidth, scale, true);
        for (i = 0; i < this.allFields.length; i++) {
          this.drawChart(this.config.lineWidth, i/this.allFields.length, false);
        }

        for (i = 0; i < this.dataSets.length; i++) {
          data = this.dataSets[i];
          this.ctx.beginPath();
          for (label in data.dataSet) {
            value = data.dataSet[label];

            descriptor = this.dataDescriptor[label];
            min = descriptor.min;
            max = descriptor.max;
            radiusX = ((value-min) / (max-min)) * (this.graphWidth*scale);
            radiusY = ((value-min) / (max-min)) * (this.graphHeight*scale);
            if (radiusX === Infinity) {
              radiusX = 0;
            }
            if (radiusY === Infinity) {
              radiusY = 0;
            }
            x = Math.floor(this.center.x + radiusX * Math.cos(descriptor.angle));
            y = Math.floor(this.center.y + radiusY * Math.sin(descriptor.angle));
            descriptor.x = x;
            descriptor.y = y;
            if (pX === undefined || pY === undefined) {
              this.ctx.moveTo(x, y);
            }
            // draw the lines
            this.ctx.lineTo(x, y);
            pX = x;
            pY = y;
          }
          this.ctx.closePath();

          //fill
          this.ctx.fillStyle = data.colorConfig.fillColor;

          this.ctx.shadowColor = data.colorConfig.glowColor;
          this.ctx.shadowBlur = 32;
          this.ctx.shadowOffsetX = 0;
          this.ctx.shadowOffsetY = 0;

          this.ctx.strokeStyle = data.colorConfig.lineColor;
          this.ctx.fill();
          this.ctx.stroke();


          for (label in data.dataSet) {
            // draw the points
            descriptor = this.dataDescriptor[label];
            this.drawPoint(descriptor.x, descriptor.y, data.colorConfig.pointSize, data.colorConfig.pointColor);
          }

        }
      }
    },

    drawPoint: {
      writable: true,

      value: function(x, y, size, color) {
        if (color === undefined)
          color = '#fff';

        if (size === undefined)
          size = 2;

        this.ctx.beginPath();
        this.ctx.moveTo(x, y);
        this.ctx.arc(x, y, size, 0, _TAU);
        this.ctx.closePath();
        this.ctx.fillStyle = color;
        this.ctx.fill();
      }
    },

    drawChart: {
      writable: true,

      value: function(lineWidth, scale, fill) {
        if (fill === undefined)
          fill = false;

        if (scale === undefined)
          scale = 1.0;

        var sectionAngle, i, j, field, startAngle, endAngle, endX, endY, previousX,
            previousY, labelX, labelY, data, value, descriptor, config, commaWidth,
            label, unitX, labelWidth, split;

        // split the base chart into the sections
        sectionAngle = _TAU / this.allFields.length;

        for (i = 0; i < this.allFields.length; i++) {
          field = this.allFields[i];
          startAngle = normalizeRadian(this.config.rotationOffset + (i*sectionAngle));
          endAngle = normalizeRadian(this.config.rotationOffset + ((i+1)*sectionAngle));

          this.dataDescriptor[field].angle = startAngle;

          endX = this.center.x + ((this.graphWidth * scale) * Math.cos(startAngle));
          endY = this.center.y + ((this.graphHeight * scale) * Math.sin(startAngle));

          this.ctx.beginPath();
          this.ctx.moveTo(this.center.x, this.center.y);
          this.ctx.lineTo(endX, endY);
          // spoke over to the next one
          endX = this.center.x + ((this.graphWidth * scale) * Math.cos(endAngle));
          endY = this.center.y + ((this.graphHeight * scale) * Math.sin(endAngle));
          this.ctx.lineTo(endX, endY);
          this.ctx.closePath();
          // antialiasing
          this.ctx.shadowColor = this.config.lineGlowColor;
          this.ctx.shadowBlur = this.config.glowSize;
          this.ctx.shadowOffsetX = 0;
          this.ctx.shadowOffsetY = 0;
          this.ctx.strokeStyle = this.config.lineColor;
          this.ctx.lineWidth = lineWidth;

          if (fill) {
            this.ctx.fillStyle = this.config.fillColor;
            this.ctx.fill();
          }

          this.ctx.stroke();

          labelX = this.center.x + (((this.graphWidth * scale) + (this.config.labelPadding * scale)) * Math.cos(startAngle));
          labelY = this.center.y + (((this.graphHeight * scale) + ((this.config.labelPadding*1.5) * scale)) * Math.sin(startAngle)) - (this.config.labelPadding/4 * scale);

          if (fill) {
            // draw labels
            // align based on placement
            split = (this.radius/2);

            // DEBUGGING
            // for (var z = 0; z < 5; z++) {
            //   this.ctx.beginPath();
            //   var tx = (this.width/2 - this.radius);
            //   this.ctx.moveTo(tx + (z*split), 0);
            //   this.ctx.lineTo(tx + (z*split), this.height);
            //   this.ctx.closePath();
            //   this.ctx.lineColor = 'white';
            //   this.ctx.stroke();
            // };

            label = this.labels[field];
            if (labelX >= (this.width/2) - split && labelX <= (this.width/2) + split) {
              this.ctx.textAlign = 'center';
              unitX = labelX - (labelWidth/2);
              if (labelY < this.height/2) {
                label.setPosition({x:labelX-Math.floor(label.element.clientWidth/2), y: labelY-label.element.clientHeight});
              } else {
                label.setPosition({x:labelX-Math.floor(label.element.clientWidth/2), y: labelY});
              }
            } else if (labelX < (this.width/2) - split) {
              this.ctx.textAlign = 'right';
              unitX = labelX - labelWidth;
              label.setPosition({x:labelX-label.element.clientWidth, y: labelY-Math.floor(label.element.clientHeight/2)});
            } else if (labelX > (this.width/2) + split) {
              this.ctx.textAlign = 'left';
              unitX = labelX;
              label.setPosition({x:labelX+Math.floor(label.element.clientWidth/2), y: labelY-Math.floor(label.element.clientHeight/2)});
            }

            label.element.classList.remove('animated-out');
          }
        }
      }
    }
  });

  return Lidar;
}();

function normalizeRadian(radian) {
  if(radian < 0 || radian > Math.PI * 2)
    return Math.abs((Math.PI * 2) - Math.abs(radian));
  else return radian;
}
